// https://codepen.io/stevn/pen/JdwNgw
// http://plnkr.co/edit/e90KNkFtjn7reDzPYtsC?p=preview
// https://gist.github.com/ericcoopey/6382449
//
// https://jsfiddle.net/derekrezek/cww9n2sy/2/
// http://bl.ocks.org/lorenzopub/820bec1dafa6a5cd11aa23c1268edcbf

function WordCloud  (elemnt, width, height) {

  this.selector = elemnt || 'body';

  // this.element = $(selector);

  this.height = height || 500;
  this.width = width || 500;

  this.wdCloud = this._wordCloud(this.selector, this.width, this.height);
}

WordCloud.prototype.wdCloud = null;
WordCloud.prototype.selector = null;
WordCloud.prototype.width = null;
WordCloud.prototype.height = null;

// Encapsulate the word cloud functionality
WordCloud.prototype._wordCloud = function (selector, width, height) {

    var fill = d3.scale.category20();

    //Construct the word cloud's SVG element
    var svg = d3.select(selector).append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
          .attr("transform", "translate("+width/2+","+height/2+")");


    //Draw the word cloud
    function draw(words) {
      var b = 1;
        var cloud = svg.selectAll("g text")
                        .data(words, function(d) { return d.text; })
                        // .data(words.map(function (d) {
                        //   return {text: d.word, size: d.weight};
                        // }));

        //Entering words
        cloud.enter()
            .append("text")
            .style("font-family", "Impact")
            .style("fill", function(d, i) {return fill(i); })
            .attr("text-anchor", "middle")
            .attr('font-size', 1)
            .text(function(d) {return d.text;});

        //Entering and existing words
        cloud
            .transition()
                .duration(600)
                .style("font-size", function(d) { return d.size + "px"; })
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .style("fill-opacity", 1);

        //Exiting words
        cloud.exit()
            .transition()
                .duration(200)
                .style('fill-opacity', 1e-6)
                .attr('font-size', 1)
                .remove();
    }

    // Helper to refactor svg and g elements size to dinamically change wordcloud size
    var refactorSizes = function (w, h){
      // Refactor svg
      $(selector+" > svg")
          .attr("width", w)
          .attr("height", h)

      // Refactor g
      svg.attr("width", w)
          .attr("height", h)
          .attr("transform", "translate("+w/2+","+h/2+")");
    }



    //Use the module pattern to encapsulate the visualisation code. We'll
    // expose only the parts that need to be public.
    return {

        //Recompute the word cloud for a new set of words. This method will
        // asycnhronously call draw when the layout has been computed.
        //The outside world will need to call this function, so make it part
        // of the wordCloud return value.
        update: function(words, w, h) {
          var wi = w || width;
          var he = h || height;

          // Update sizes of containers with new width/height
          if (wi != svg.attr("width") || he != svg.attr("height")) { refactorSizes(wi,he); }

          d3.layout.cloud().size([wi, he])
              .words(words)
              .padding(2)
              .rotate(function() { return 0; })
              .font("Impact")
              .fontSize(function(d) { return d.size; })
              .on("end", draw)
              .start();
        }
    }
}

WordCloud.prototype.showNewWords = function (words, w, h) {
    this.wdCloud.update(words, w, h);
}
