function Skills (options){

  if (options)  this.options =  $.extend(true, {}, this.options, options || {});
  this.sizeHelper.sk = this;
}

Skills.prototype.options = {
  element : document.getElementById("skills"),
  autoplay : {
    autostart : false,
    visible: true,
    timeout: 2000
  },
  showAllBtn : {
    text : "ShowAll",
    visible: true
  },
  skillLabel : { // Shows a label with the name of the selected skill
    visible : true
  },
  defaultSkill : ""

}



Skills.prototype.skillsObject = {
  Frontend : [
    {text:"Qt",size:60},
    {text:"Qml",size:50},
    {text:"Javascript",size:55},
    {text:"Jquery",size:50},
    {text:"AngularJs",size:50},
    {text:"Html&Css",size:40},
    {text:"JavaFX",size:40},
    {text:"Gulp",size:30}],
    // And various frameworks and libraries

  Backend : [
    {text:"Java",size:60},
    {text:"Spring",size:50},
    {text:"Maven",size:40},
    {text:"NodeJs",size:40},
    {text:"MySql",size:50},
    {text:"Postgress",size:50},
    {text:"Hibernate",size:40},
    {text:"MongoDB",size:20}],

  SysAdmin : [
    {text:"Linux",size:70},
    {text:"Windows",size:40},
    {text:"WebServers",size:50},
    {text:"VPN",size:50},
    {text:"Bash",size:60}],

  PhoneApps : [
    {text:"Android",size:60},
    {text:"Qml",size:50},
    {text:"AngularJs",size:50},
    {text:"Qt",size:50},
    {text:"ObjectiveC",size:20}],

  WebCMS : [
    {text:"Joomla",size:60},
    {text:"Wordpress",size:50},
    {text:"Matomo",size:40},
    {text:"NextCloud",size:40},
    {text:"Drupal",size:20}],

  More : [
    {text:"Arduino",size:40},
    {text:"Java",size:60},
    {text:"Python",size:50},
    {text:"Git",size:50},
    {text:"C#",size:50}],

  // Get a concatenation of all without repeated words
  all : function (){
    var con = new Array();
    // Function that check if con contain previous object.
    // For some reason Array.includes doesn't work fine.
    var contain = function (v){
      for (var x = 0 ; x < con.length; x++) {
        if (con[x].text == v.text) return true;
      }
      return false;
    }
    // Loop each key
    for (var k in this) {
      if ( k.startsWith("all") ) continue;
      else if (this.hasOwnProperty(k) ) {
        var tmp = [];
        // For each key get each object
        for (var i=0 ; i<this[k].length ; i++) {
          // And check if this objets already exists on con array
          if (!contain(this[k][i])){
            // If not exists add to it
            tmp = tmp.concat(this[k][i]);
          }
        }
        con = con.concat(tmp);
      }
    }
    return con;
  },
  // Get all key names without keys started with word "all"
  allKeyNames : function (){
    var res = [];
    for (var k in this) {
      if ( k.startsWith("all") ) continue;
      res.push(k);
    }
    return res;
  }
};

Skills.prototype.charts = null;
Skills.prototype.wc = null;
Skills.prototype.wcContainer = null;
Skills.prototype.selectedSkill;
Skills.prototype.skillName;
Skills.prototype.autoplayObject;

Skills.prototype.start = function (){
  var self = this;

  // Insert html code
  this.options.element.innerHTML = this.htmlbody;
  var shwall = document.getElementById("showAll");
  var playBtn = document.getElementById("pauseSkills")
  var skillLabel = document.getElementById("skillLabel");
  this.skillName = document.getElementById("skillName");
  if (this.options.showAllBtn.visible || this.options.skillLabel.visible || this.options.autoplay.visible){
    // Show all  button
    if (this.options.showAllBtn.visible) {
      shwall.style.visibility = 'visible';
      shwall.innerHTML = this.options.showAllBtn.text;
      shwall.onclick = function (){
        self.showAll();
      }
    } else {
      shwall.style.display = 'none';
    }
    // Autoplay button
    if (this.options.autoplay.visible) {
      playBtn.style.visibility = 'visible';
    } else {
      playBtn.style.display = 'none';
    }
    // Get div where skill name will be shown
    if (this.options.skillLabel.visible) {
      skillLabel.style.visibility = 'visible';
    } else {
      skillLabel.style.display = 'none';
    }
  } else {
    document.getElementById("cloudHeader").style.display = 'none';
  }

  // Start radar chart
  this.charts = new SkillCharts(document.getElementById("globalSkills"), this);
  this.charts.startCharts(this.skillsObject.allKeyNames());

  // Start wordclouds
  // the width of worcloud must to be the size of the parent div
  this.wcContainer = $("#wordCloud");
  var wcwidth = this.wcContainer.width();
  // And the height must to augment on mobile devices
  // Smalest resolutions of width 500px will need more height for display all words
  var height = this.sizeHelper.getLittleHeight();
  this.wc = new WordCloud('#wordCloud', wcwidth, height)

  // Create autoplay elemnt with pause/start button
  this.autoplayObject = new Autoplay(this);
  if (this.options.autoplay.autostart) this.autoplayObject.play();
  else if ( this.options.defaultSkill in this.skillsObject) {
    this.setSkill(Object.keys(this.skillsObject).indexOf(this.options.defaultSkill))
  }
}

// Set the selected skill and show wordcloud
Skills.prototype.setSkill = function (i){
  var self = this;
  if (self.selectedSkill != i ){
    self.selectedSkill = i;
    // Set word cloud
    // var wordArray = self.skillsObject[Object.keys(self.skillsObject)[i]];
    var wordArray = self.sizeHelper.getWords(i);
    if (this.wcContainer.height() != this.wc.height) this.wcContainer.height(this.wc.height);
    self.wc.showNewWords(wordArray, this.wcContainer.width(), this.wc.height);
    // Change skill name on div element
    this.skillName.innerHTML = Object.keys(self.skillsObject)[i];
  }
}

// Function that show all the skills in an unique wordcloud
Skills.prototype.showAll = function () {

  // New w/h of the all cloud (bigger than the others)
  var width = this.wcContainer.width();
  var newHeight = this.sizeHelper.getBigHeight();

  // Pause autoplay if is runing
  if (!this.autoplayObject.stopped) {
    this.autoplayObject.stop();
  }

  // Set no selectedSkill
  this.charts.closeTip(0, this.selectedSkill);
  this.selectedSkill = null;

  // Set the wordcloud height bigger
  this.wcContainer.height(newHeight);

  // Get all words and pass it to wordcloud
  // var wordArray = this.skillsObject.all();
  var wordArray = this.sizeHelper.getWords();
  this.wc.wdCloud.update(wordArray, width, newHeight);

  // Change skill name on div element
  this.skillName.innerHTML = "Todo";
}

// Helper to get height of wordcloud, or change the size of the words...
Skills.prototype.sizeHelper = {
  // Get height for windows width under 500px for little wordclouds
  getLittleHeight : function (){
    if (this.isLittleWindow()) {
      return 250;
    }
    return 220;
  },
  // Get height for windows width under 500px for big wordclouds
  getBigHeight : function (){
    if (this.isLittleWindow()) {
      return 900;
    } else if (this.isMediumWindow()) {
      return 775;
    }
    return 675;
  },
  // Get words of key in x position
  // Or all words if req="all" or undefined
  getWords : function (req) {
    var wa = []; // Word array
    // Get word array
    if (req  === "all" || typeof req === 'undefined') {
      wa = this.sk.skillsObject.all();
    } else if (req >= 0 && req <= 5) {
      wa = this.sk.skillsObject[Object.keys(this.sk.skillsObject)[req]];
    }
    // Set correct size of words if needed
    if (this.isLittleWindow()) {
      return this.getResizedWords(wa, -10);
    }
    return wa;

  },
  // On a wordsArray, get size property and apply the resizeFactor
  getResizedWords : function (wordsArray, resizeFactor){
    var tmp =  wordsArray.map(u => Object.assign({}, u, { approved: true }));
    for (var i = 0 ; i < tmp.length ; i++){
      tmp[i].size = tmp[i].size+resizeFactor;
    }
    return tmp;
  },
  isLittleWindow : function (){
    if (window.screen.width <= 500) {
      return true;
    }
    return false;
  },
  isMediumWindow : function (){
    if (window.screen.width <= 900 && window.screen.width > 500) {
      return true;
    }
    return false;
  }
}


var Autoplay = function (skills) {

  var self = this;
  this.skills = skills;

  // Create the pause autoplay button and bind it to self.stopped
  // The button is composed by a div and unvisible checkbox.
  // We bind the checkbox with the self.stopped but we handle the entire div onclick
  var checkbox = document.getElementById("playpausecheck");
  var div = document.getElementById("pauseSkills");

  //https://namitamalik.github.io/2-way-data-binding-in-Plain-Vanilla-JavaScript/
  //https://stackoverflow.com/questions/18524652/how-to-use-javascript-object-defineproperty
  div.onclick = function (evt) {
    if (checkbox.checked) {
      checkbox.checked = false;
    } else {
      checkbox.checked = true;
    }
    self.stopped = checkbox.checked;
  }
  Object.defineProperty(this, "stopped", {
    set: function (newValue) {
      if (checkbox.checked != newValue) checkbox.checked=newValue;
      _stopped = newValue;
      if (_stopped == false) startAutoplay();
    },
    get: function () { return _stopped;}
  });

  // Function that starts the loop of autoplay
  var startAutoplay = function (){
    var skills = self.skills;

    function loop(i) {
      if (!self.stopped) {
        i = i || 0;
        // Close previous tip
        if (skills.selectedSkill >= 0) {
          skills.charts.closeTip(0, skills.selectedSkill)
        }
        // Open new tip and select actual skill
        skills.setSkill (skills.charts.openTip( 0, i))

        // Max iterations
        if ( i == 5 ) i=-1;

        setTimeout(function() { loop( i + 1)}, self.skills.options.autoplay.timeout)
      }

    }
    loop(skills.selectedSkill);
  }

  // Initialize autoplay as stopped
  var _stopped = true; // private member
  this.stopped = _stopped;

}

Autoplay.prototype.stop = function (){
  this.stopped = true;
}
Autoplay.prototype.play = function (){
  this.stopped = false;
}

Skills.prototype.htmlbody = `
<div class="container">
  <div class="row">
    <div class="col-md-1"><a href="#"><i class="fa fa-3x fa-fw fa-file-code-o"></i></a></div>
  </div>
  <div class="row" >
    <div class="col-md-4" style="margin-bottom: 15px;">
      <div class="chart-container">
        <canvas id="globalSkills" width="50" height="50"></canvas>
      </div>
    </div>
    <div class="col-md-8">

      <row id="cloudHeader">
        <div class="col-md-4 text-center" style="margin-top: 5px;">
          <div id="showAll" class="showall btn btn-lg">
            <span>Show All</span>
          </div>
          <span class="playpause ">
            <input  type="checkbox" value="None" id="playpausecheck" name="check" />
            <label id="pauseSkills" for="playpause" tabindex=1></label>
          </span>
        </div>

        <div id="skillLabel" class="col-md-4 text-center">
          <h3  id="skillName" style="color:#666666;font-weight: bold;"></h3>
          <hr>
        </div>
      </row>

      <row>
        <div id="wordCloud" ></div>
      </row>
    </div>

  </div>
</div>
`
