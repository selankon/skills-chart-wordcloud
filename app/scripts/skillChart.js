function SkillCharts (cv, skills) {
  this.cv = cv || document.getElementById("globalSkills");
  this.skills = skills || null;
}

SkillCharts.prototype.cv = null;
SkillCharts.prototype.chart = null;
SkillCharts.prototype.selected = "Frontend";
SkillCharts.prototype.skills = null;


SkillCharts.prototype.startCharts = function (labels) {
  var cv = this.cv;
  var ctx = cv.getContext('2d');

  var self = this;

  var data = {
    labels: labels,
    datasets: [{
        label: 'Skills',
        backgroundColor: "rgba(214, 236, 251,0.5)",
        borderColor: 'rgba(53, 162, 235)',
        pointStyle: "circle",
        pointBorderColor: "#FFF",
        pointBackgroundColor: '#05436d',

        data: [9, 8.5, 6.5, 7, 8.5, 7]

    }]
  }


  var options = {
    maintainAspectRatio: false,
    responsive: true,

    title: {
      display: true,
      position: 'top',
      text: 'Skills',
      fontSize: 25,
    },
    scale: {
        // Hides the scale
        // display: true,
        // reverse: true,
        ticks: {
            beginAtZero: true,
            max: 10,
            maxTicksLimit: 5,
        },
        pointLabels: {
            fontSize: 17,
            callback: function(value, index, values) {
              if (value == self.selected) {
                // return value.toUpperCase();
              }
              return value;
            }
        }
    },
    legend: {
      display: false,
      labels: {
        // usePointStyle: true,
        // fontSize: 20,
      }
    },
    tooltips: {
      positionMode: 'average',
      intersect: false,
      callbacks: {
        label: function(tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || '';
              // self.selected = data.labels[tooltipItem.datasetIndex] || '';

              if (self.skills.autoplayObject.stopped) {
                self.skills.setSkill(tooltipItem.index);
              }

               if (label) {
                   label += ': ';
               }
               label += Math.round(tooltipItem.yLabel * 100) / 100;

               return label;
         }

      }
    },
    events:["mousemove", "mouseout", "click", "touchstart", "touchmove"],

    onAnimationComplete: function()
    {
        this.showTooltip([this.datasets[0].points[your_point]], true);
    },
    tooltipEvents: []

  };

  this.chart = new Chart(ctx, {
      type: 'radar',
      data: data,
      options: options
  });



  // globalSkillsChart.data.datasets[0].borderColor = '#f39c12';
  // globalSkillsChart.update();

  var chart = this.chart;
  cv.onclick = function (evt) {
    // https://jsfiddle.net/bencekd/1u5nuvc5/5/
      var activePoint = chart.getElementAtEvent(evt);
  }

  cv.onmouseenter = function(e) {
    if (!self.skills.autoplayObject.stopped) {
      self.skills.autoplayObject.stop();
    }
  }

  cv.onmouseleave = function (e) {
    self.openTip( 0, self.skills.selectedSkill)

  }

}


SkillCharts.prototype.openTip = function (datasetIndex,pointIndex) {
    if(this.chart.tooltip._active == undefined)
       this.chart.tooltip._active = []
    var activeElements = this.chart.tooltip._active;
    var requestedElem = this.chart.getDatasetMeta(datasetIndex).data[pointIndex];

    for(var i = 0; i < activeElements.length; i++) {
        if(requestedElem._index == activeElements[i]._index)
           return;
    }
    activeElements.push(requestedElem);
    this.chart.tooltip._active = activeElements;

    this.chart.tooltip.update();
    this.chart.draw();

    return pointIndex;
}

SkillCharts.prototype.closeTip = function (datasetIndex,pointIndex){
   var activeElements = this.chart.tooltip._active;
   if(activeElements == undefined || activeElements.length == 0)
     return;
   var requestedElem = this.chart.getDatasetMeta(datasetIndex).data[pointIndex];
   for(var i = 0; i < activeElements.length; i++) {
       if(requestedElem._index == activeElements[i]._index)  {
          activeElements.splice(i, 1);
          break;
       }
   }
   this.chart.tooltip._active = activeElements;
   this.chart.tooltip.update(true);
   this.chart.draw();
}
