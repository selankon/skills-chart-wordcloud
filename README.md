# Skills Chart WordCloud

Using the combination of [chart.js](https://www.chartjs.org/) and [d3-cloud](https://github.com/jasondavies/d3-cloud) libraries it dinamically generate a [radar chart](https://www.chartjs.org/docs/latest/charts/radar.html) where each of the labels open a wordcloud generated with keywords related to the label.

![](wololo.gif)

It has an **autoplay function** that loop into all the chart labels and a **show all** button that shows you all the words of all the features.

#### TODO

* [ ] Implement external json file for the skill configuration and add it as configurable option
* [ ] Check for bugs when change from autoplay to other things. It throw weeoes

#### Features

* [x] Autoplay
* [x] Show All button
* [x] On hover wordcloud generator
* [x] Responsive
* [x] Word Cloud dynamic height
* [x] Json based skills list

#### Usage
To use it, import all project to yours and call the javascript:
```javascript
var skills = new Skills({options});
skills.start();
```

#### Accepted options

```javascript
{
  element : document.getElementById("skills"),
  autoplay : {
    autostart : false,      // Start autoplay on load
    visible: true,          // Show autoplay button
    timeout: 2000           // Timout for the loop
  },
  showAllBtn : {
    text : "ShowAll",       // Show all button text
    visible: true
  },
  skillLabel : {            // Shows a label with the name of the selected skill
    visible : true
  },
  defaultSkill : ""         // String with the default skill to show
}
```

#### Json skills example
```
{
  Frontend : [
    {text:"Qt",size:60},
    {text:"Qml",size:50},
    {text:"Javascript",size:55},
    {text:"Jquery",size:50},
    {text:"AngularJs",size:50},
    {text:"Html&Css",size:40},
    {text:"JavaFX",size:40},
    {text:"Gulp",size:30}],

  Backend : [
    {text:"Java",size:60},
    {text:"Spring",size:50},
    {text:"Maven",size:40},
    {text:"NodeJs",size:40},
    {text:"MySql",size:50},
    {text:"Postgress",size:50},
    {text:"Hibernate",size:40},
    {text:"MongoDB",size:20}],

  SysAdmin : [
    {text:"Linux",size:70},
    {text:"Windows",size:40},
    {text:"WebServers",size:50},
    {text:"VPN",size:50},
    {text:"Bash",size:60}],

  PhoneApps : [
    {text:"Android",size:60},
    {text:"Qml",size:50},
    {text:"AngularJs",size:50},
    {text:"Qt",size:50},
    {text:"ObjectiveC",size:20}],

  WebCMS : [
    {text:"Joomla",size:60},
    {text:"Wordpress",size:50},
    {text:"Matomo",size:40},
    {text:"NextCloud",size:40},
    {text:"Drupal",size:20}],

  More : [
    {text:"Arduino",size:40},
    {text:"Java",size:60},
    {text:"Python",size:50},
    {text:"Git",size:50},
    {text:"C#",size:50}]
}

```
